#!/usr/bin/env bash
usage() {
    NAME=$(basename $0)
    cat <<EOF
${NAME} <outputs>

Wrapper for RSEM quantification from STAR alignments

example runs:
./RunningRSEM.sh 

EOF

}

source ./variables.sh

if [ ! -d ${BASEDIR}/rsem ]; then
    mkdir -p ${BASEDIR}/rsem
fi

QUANTLOC=${BASEDIR}/rsem
ALIGNLOC=${BASEDIR}/star

NOW=$(date +"%Y-%m-%d")
LOGFILE=../results/RSEMquantification-$NOW.log

&>${LOGFILE}
echo [`date`] $0 $@ >> ${LOGFILE}

for ((i=0; i<=${#samples[@]}-1; i++ )); do
    sample=${samples[$i]}
    echo "$sample"
    echo -e [`date`] "${sample}" >> ${LOGFILE}

    ## Converting BAM to RSEM bam
    echo "Converting to RSEM file and validating"
    echo "Converting to RSEM file and validating" >> ${LOGFILE}
    $RSEM/convert-sam-for-rsem --temporary-directory ${TEMPLOC} \
	${ALIGNLOC}/${sample}/${sample}-Transcriptome-sorted.bam \
	${TEMPLOC}/${sample}-Transcriptome-RSEM
   
    ## Isoform quantification
    echo "Isoform quantification"
    echo "Running RSEM" >> ${LOGFILE}

    $RSEM/rsem-calculate-expression --num-threads 12 \
    	--bam \
    	--paired-end \
    	--no-bam-output \
    	--forward-prob 0.5 \
    	--seed 11111 \
    	${TEMPLOC}/${sample}-Transcriptome-RSEM.bam $RSEMGENOME \
    	${TEMPLOC}/${sample}

    echo "copying to alignment dir" >> ${LOGFILE}
    rsync -av ${TEMPLOC}/ ${QUANTLOC}/$sample

    echo "removing files in tmp" >> ${LOGFILE}
    rm ${TEMPLOC}/*.*
    rm -rf ${TEMPLOC}/*.stat

done

echo [`date`] "DONE" >> ${LOGFILE}
