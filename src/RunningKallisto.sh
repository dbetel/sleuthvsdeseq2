#!/bin/bash -l
usage() {
    NAME=$(basename $0)
    cat <<EOF
${NAME} <outputs>

Wrapper for RSEM quantification from STAR alignments

example runs:
./RunningKallisto.sh 

EOF

}

source ./variables.sh
export LD_LIBRARY_PATH=/home/luis/pandasoft/gcc/gcc-4.8.4/lib64:$LD_LIBRARY_PATH
slchoose -q hdf5 1.8.2 gcc4_64

if [ ! -d ${BASEDIR}/kallisto ]; then
    mkdir -p ${BASEDIR}/kallisto
fi

QUANTLOC=${BASEDIR}/kallisto

NOW=$(date +"%Y-%m-%d")
LOGFILE=../results/KallistoQuantification-$NOW.log

&>${LOGFILE}
echo [`date`] $0 $@ >> ${LOGFILE}

for ((i=0; i<=${#samples[@]}-1; i++ )); do
    sample=${samples[$i]}
    echo "$sample"
    echo -e [`date`] "${sample}" >> ${LOGFILE}

    size1=(`wc -l < ${FASTQLOC}/${sample}_1.fastq`)
    size2=(`wc -l < ${FASTQLOC}/${sample}_2.fastq`)
    echo -n "Number of R1 reads " >> ${LOGFILE}
    echo $size1/4 | bc >> ${LOGFILE}

    echo -n "Number of R2 reads " >> ${LOGFILE}
    echo $size2/4 | bc >> ${LOGFILE}


    ## Alignment
    echo "Kallisto"
    echo "Running Kallisto" >> ${LOGFILE}
    $KALLISTO quant -i $KALLISTOGENOME \
    	-o ${TEMPLOC} \
    	--pseudobam -t 6 -b 150 \
    	${FASTQLOC}/${sample}_1.fastq ${FASTQLOC}/${sample}_2.fastq >${TEMPLOC}/${sample}_pseudo.sam
    
    echo "Samtools"
    echo "Running samtools" >> ${LOGFILE}
    samtools view -S -b ${TEMPLOC}/${sample}_pseudo.sam -o ${TEMPLOC}/${sample}_pseudo.bam
    samtools sort ${TEMPLOC}/${sample}_pseudo.bam ${TEMPLOC}/${sample}_pseudo-sorted
    samtools index ${TEMPLOC}/${sample}_pseudo-sorted.bam
    samtools flagstat ${TEMPLOC}/${sample}_pseudo-sorted.bam >> ${LOGFILE}

    echo "copying to alignment dir" >> ${LOGFILE}
    rsync -av ${TEMPLOC}/abundance.* ${QUANTLOC}/$sample
    rsync -av ${TEMPLOC}/run_info.json ${QUANTLOC}/$sample
    rsync -av ${TEMPLOC}/${sample}_pseudo-sorted.bam* ${QUANTLOC}/$sample
    
    echo "removing files in tmp" >> ${LOGFILE}
    rm ${TEMPLOC}/*.*
    rm -rf ${TEMPLOC}/Sample_$sample

done

echo [`date`] "DONE" >> ${LOGFILE}
