#!/bin/sh
# example
# ./FetchGEO.sh 
# Input:
# 1. sra file
# 2. FASTQ quality offset value [64, 33] 64=old illumina 33=standard value and CASAVA1.8
##
# ./FetchGEO.sh 
##

fastqDump=/home/dob2014/lib/sratoolkit.2.5.4-1-centos_linux64/bin/fastq-dump
##
## run fastq-dump
echo "runnting fastq-dump"
filePath="/zenodotus/dat01/betellab_scratch/dob2014/SleuthTemp/fastq"
declare -a accessions
readarray -t accessions < ../data/SRR_Acc_List.txt
echo "Fetching ${accessions[@]}"
${fastqDump} -Q 33 --split-files -O ${filePath} ${accessions[@]}
