#!/usr/bin/env bash
usage() {
    NAME=$(basename $0)
    cat <<EOF
${NAME} <outputs>

Wrapper for RSEM quantification from STAR alignments

example runs:
./RunningRSEM.sh 

EOF

}

source ./variables.sh

QUANTLOC=${BASEDIR}/rsem

NOW=$(date +"%Y-%m-%d")


echo [`date`] $0 $@ >> ${LOGFILE}
counts=()
for ((i=0; i<=${#samples[@]}-1; i++ )); do
    sample=${samples[$i]}
    echo "$sample"
    
    ## isoform counts
    # counts[$i]=${QUANTLOC}/${samples[$i]}/${samples[$i]}.isoforms.results

    ## gene counts
    counts[$i]=${QUANTLOC}/${samples[$i]}/${samples[$i]}.isoforms.results

done

## join array to a single string
files=$(printf "%s " "${counts[@]}")
$RSEM/rsem-generate-data-matrix ${files} > ../data/counts_${NOW}.matrix