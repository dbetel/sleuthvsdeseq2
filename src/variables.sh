#!/bin/bash -l

## Sample list of samples
declare -a samples
readarray -t samples < ../data/SRR_Acc_List.txt

FASTQLOC="/zenodotus/dat01/betellab_scratch/dob2014/SleuthTemp/fastq"
REFGENOME="/scratch001/dob2014/genomes/hg19_starGenomeGencode19"
RSEMGENOME="/scratch001/dob2014/genomes/hg19_rsemGenomeGencode19/rsemGenomeGencode"
KALLISTOGENOME="/scratch001/dob2014/genomes/hg19_kallistoGenomeGencode19/gencode.v19.pc_transcripts.idx"
BASEDIR="/zenodotus/dat01/betellab_scratch/dob2014/SleuthTemp"
GENCODEGTF="/scratch001/dob2014/gencode/gencode.v19.annotation.gtf"
TEMPLOC="/scratch001/dob2014/tmp/Arya"

STAR=/home/dob2014/lib/STAR-STAR_2.4.0j/source/STAR
FASTX=/home/dob2014/lib/fastx_bin
FLEXBAR=/home/dob2014/lib/flexbar_v2.4_linux64/flexbar
RSEM=/home/dob2014/lib/rsem-1.2.22
FEATURE_COUNTS=/home/dob2014/lib/subread-1.4.2-source/bin/featureCounts
KALLISTO=/home/dob2014/lib/kallisto-0.42.3/bin/kallisto

export PATH=/home/dob2014/lib/samtools-0.1.19:/home/dob2014/lib/boost_1_49_0/lib:$PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/dob2014/lib/boost_1_49_0/lib

ADDRESS=dob2014@med.cornell.edu    

