#!/usr/bin/env bash
usage() {
    NAME=$(basename $0)
    cat <<EOF
${NAME} <outputs>

Wrapper for STAR aligner
RunningStar.sh
example runs:
./RunningStar.sh 

EOF

}

source ./variables.sh

if [ ! -d ${BASEDIR}/star ]; then
    mkdir -p ${BASEDIR}/star
fi

ALIGNLOC=${BASEDIR}/star

NOW=$(date +"%Y-%m-%d")
LOGFILE=../results/STARAlignment-$NOW.log

&>${LOGFILE}
echo [`date`] $0 $@ >> ${LOGFILE}

for ((i=0; i<=${#samples[@]}-1; i++ )); do
    sample=${samples[$i]}
    echo "$sample"
    echo -e [`date`] "${sample}" >> ${LOGFILE}
    
    size1=(`wc -l < ${FASTQLOC}/${sample}_1.fastq`)
    size2=(`wc -l < ${FASTQLOC}/${sample}_2.fastq`)
    echo -n "Number of R1 reads " >> ${LOGFILE}
    echo $size1/4 | bc >> ${LOGFILE}

    echo -n "Number of R2 reads " >> ${LOGFILE}
    echo $size2/4 | bc >> ${LOGFILE}


    ## Alignment
    echo "Alignment"
    echo "Running STAR" >> ${LOGFILE}
    $STAR --genomeDir ${REFGENOME} --readFilesIn ${FASTQLOC}/${sample}_1.fastq ${FASTQLOC}/${sample}_2.fastq \
    	--runThreadN 12 \
    	--outFileNamePrefix ${TEMPLOC}/ \
    	--quantMode TranscriptomeSAM \
    	--outFilterMismatchNmax 1 >> ${LOGFILE}

    echo "Samtools"
    echo "Running samtools" >> ${LOGFILE}
    samtools view -S -b ${TEMPLOC}/Aligned.out.sam -o ${TEMPLOC}/${sample}.bam
    samtools sort ${TEMPLOC}/${sample}.bam ${TEMPLOC}/${sample}-sorted
    samtools index ${TEMPLOC}/${sample}-sorted.bam
    samtools flagstat ${TEMPLOC}/${sample}-sorted.bam >> ${LOGFILE}
    
    samtools sort ${TEMPLOC}/Aligned.toTranscriptome.out.bam ${TEMPLOC}/${sample}-Transcriptome-sorted
    samtools index ${TEMPLOC}/${sample}-Transcriptome-sorted.bam

    echo "removing intermediate files" >> ${LOGFILE}
    rm ${TEMPLOC}/Aligned.out.sam
    rm ${TEMPLOC}/${sample}.bam
    rm ${TEMPLOC}/Aligned.toTranscriptome.out.bam
    rm -rf ${TEMPLOC}/Sample_${sample}

    echo "copying to alignment dir" >> ${LOGFILE}
    rsync -av ${TEMPLOC}/*.* ${ALIGNLOC}/temp


    rename ${sample} ${samples[$i]} ${ALIGNLOC}/temp/${sample}*
    mv ${ALIGNLOC}/temp ${ALIGNLOC}/${samples[$i]}

    echo "removing files in tmp" >> ${LOGFILE}
    rm ${TEMPLOC}/*.*
    
    SUBJECT="[STAR]: sample ${sample} Done"
    MAIL_TXT="Aligning ${sample} to hg19 is complete"
    echo "$MAIL_TXT" | mail -s "$SUBJECT" $ADDRESS

done

## removing genome from memory
# $STAR --genomeDir ${REFGENOME} --genomeLoad Remove
echo [`date`] "DONE" >> ${LOGFILE}
