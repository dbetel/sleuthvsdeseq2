# README #

Comparison of two isoform differential expression (DE) pipelines.

1. STAR aligner + RSEM quantification + DESeq2 for DE
2. Kallisto aligner and quantification + Sleuth for DE

### How do I get set up? ###

* R code requires DESeq2 and Sleuth R packages. Currently [Sleuth] (http://pachterlab.github.io/sleuth/) can only be installed on R3.2.1.
* This comparison is based on example data set available at GEO, accession GSE37704. 
* Before running any scripts edit the paths and software variables in `variables.sh` to match your run environment.
* To download FASTQ files run `FetchGEO.sh`
* Pipeline 1: `RunningStar.sh` + ` RunningRSEM.sh` + `GenerateDataMatrixRSEM.sh` + `DE.R` + (optional) `ClusterSamples.R`
* Pipeline 2: `RunningKallisto.sh` + `sleuthDE.R`
* Comparing TPMs and DE: `CompareSleuthDESeq2.R` 

### Who do I talk to? ###

* Repo admin: Doron Betel (dbetel) 
* Group members: soccin, Raya Khanin, Nicolas Robine
